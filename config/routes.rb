Rails.application.routes.draw do
  root 'pages#home'
  get 'pages/home'

  get 'pages/about'

  get 'pages/team'

  resources :rooms
  resources :events

  post '/rooms' => 'rooms#create'
  # patch '/rooms' => 'rooms#update'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
