class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.datetime :time_in
      t.datetime :time_out
      t.integer :room_id
      t.string :phone

      t.timestamps
    end
  end
end
