class CreateRooms < ActiveRecord::Migration[5.1]
  def change
    create_table :rooms do |t|
      t.string :name
      t.datetime :last_use_at
      t.boolean :occupied

      t.timestamps
    end
  end
end
