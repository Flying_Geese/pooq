class RoomsController < ApplicationController
  before_action :find_room, only: [:show, :edit]


  def index
    @rooms = Room.all
    json_response(@rooms)
  end

  def show
    json_response(@room)
  end

  def create
   @room = Room.find(params[:room_id])
    if @room.occupied?
      @reply = "This bathroom is currently in use. Please hold it."
      json_response(@reply)
    else
      @reply = "THANK YOU FOR SHITTING WITH US!"
      @room.occupied = true
      @room.save
      json_response(@reply)
    end
  end

  def enter
    @room = Room.find(params[:room_id])
    if @room.occupied?
      @reply = "This bathroom is currently in use. Please hold it."
      json_response(@reply)
    else
      @reply = "THANK YOU FOR SHITTING WITH US!"
      @room.occupied = true
      @room.save
      json_response(@reply)
    end
  end

  def exit
    @room = Room.find(params[:id])
    @room.occupied = false
    @room.last_used_at = Time.now
    @room.save
  end

  def update
    p params
    @room = Room.find(params[:id])
    p @room.errors.full_messages
    if @room.occupied?
      # @reply = "This bathroom is currently in use. Please hold it."
      @room.occupied = false
      @room.save
      json_response(@reply)
    else
      @reply = "THANK YOU FOR SHITTING WITH US!"
      @room.occupied = true
      @room.save
      json_response(@reply)
    end
  end


  private

  def find_room
    @room = Room.find(params[:id])
  end


end
